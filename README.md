# Tricot

Tricot is a reverse-proxy for exposing your services via TLS that integrates well with Consul and Nomad.

Tricot does the following things:

- Find the services running on your cluster that expose an HTTP or HTTPS access point, by listing the Consul catalog
- Request TLS certificats via Let's Encrypt for the domain names you are exposing
- Store these certificates in Consul's key-value store
- Renew these certificates when they reach half of their designated lifetime
- Expose an HTTPS port where all of the requests are forwarded to one of the back-end services, based on matching rules (see below)
- Expose an HTTP port that redirects everything to the HTTPS port (the HTTP port is mandatory as it is also used for Let's Encrypt validation)
- Load-balance incoming requests among different available backends, or redirect all requests to the best-available matching backend, depending on desired configuration
- Optionnally, compress data comming from the backend before forwarding it to the client

## Configuring Tricot

Tricot uses the following environment variables for its configuration (they can also be passed as command line arguments, see `--help`):

- `TRICOT_LETSENCRYPT_EMAIL` (**mandatory**): the e-mail address used to register a Let's encrypt account
- `TRICOT_NODE_NAME` (**mandatory**): the name in Consul of the node on which this instance of Tricot is running
- `TRICOT_CONSUL_HOST` (default: `http://127.0.0.1:8500`): the HTTP or HTTPS address at which the Consul catalog can be reached
- `TRICOT_CONSUL_CA_CERT`, `TRICOT_CONSUL_CLIENT_CERT`, `TRICOT_CONSUL_CLIENT_KEY` (optional): parameters necessary to access Consul over HTTPS when cluster TLS is enabled
- `TRICOT_CONSUL_KV_PREFIX` (default: `tricot/`): the prefix in Consul's key-value store under which Tricot stores its data (the certificates it obtained from Lets Encrypt)
- `TRICOT_HTTP_BIND_ADDR` (default: `0.0.0.0:80`): the HTTP address and port on which Tricot listens
- `TRICOT_HTTPS_BIND_ADDR` (default: `0.0.0.0:443`): the HTTPS address and port on which Tricot listens
- `TRICOT_ENABLE_COMPRESSION` (default: `false`): whether to enable transparent compression of data coming back from backends
- `TRICOT_COMPRESS_MIME_TYPES` (default: `text/html,text/plain,text/css,text/javascript,text/xml,application/javascript,application/json,application/xml,image/svg+xml,font/ttf`): comma-separated list of MIME types for which Tricot will do compression

[Here is an example of how to run Tricot as a Nomad service](https://git.deuxfleurs.fr/Deuxfleurs/nixcfg/src/branch/main/app/frontend/deploy/frontend-tricot.hcl) (in this example it also integrates with [Diplonat](https://git.deuxfleurs.fr/Deuxfleurs/diplonat)).

## Adding and priorizing backends

Backends are configured by adding tags of the following form to the services in Consul:

- `tricot myapp.example.com`: indicates that Tricot should get a TLS certificate for domain myapp.example.com and route traffic for this domain to this service
- `tricot myapp.example.com 10`: same but with a priority of 10. This is lower than the default priority value of 100, so Tricot will only use this rule if no higher-priority services are available.
- `tricot myapp.example.com/path/to_subresource`: same but indicates that only requests whose paths start with `/path/to/subresource` should be routed to this service
- `tricot myapp.example.com/path/to_subresource 10`: combining the previous two examples
- `tricot-https myapp.example.com`: same, but indicates that the backend service handling the request expects an HTTPS request and not an HTTP request. In this case, Tricot will do everything in its power to NOT verify the backend's TLS certificate (ignore self-signed certificate, ignore TLS hostname, etc).
- `tricot-add-header Access-Control-Allow-Origin *`: add the `Access-Control-Allow-Origin: *` header to all of the HTTP responses when they are proxied back to the client
- `tricot-global-lb`: load-balance incoming requests to all matching backends
- `tricot-site-lb`: load-balance incoming requests to all matching backends that are in the same site (geographical location); when site information about nodes is not available, this is equivalent to `tricot-global-lb`

Any number of such rules can be combined freely.

[Here](https://git.deuxfleurs.fr/Deuxfleurs/nixcfg/src/branch/main/app/dummy/deploy/dummy-nginx.hcl)
[are](https://git.deuxfleurs.fr/Deuxfleurs/nixcfg/src/branch/main/app/garage-staging/deploy/garage.hcl)
[some](https://git.deuxfleurs.fr/Deuxfleurs/nixcfg/src/branch/main/app/im/deploy/im.hcl)
[examples](https://git.deuxfleurs.fr/Deuxfleurs/infrastructure/src/branch/main/app/jitsi/deploy/jitsi.hcl).

## How Tricot matches backends

Tricot priorizes backend nodes in the following order:

1. Select rule with the highest explicitly set priority value
2. Select rule with the longest path prefix that matches the incoming request
3. Select rule that redirects request to the same node Tricot is running on, except `tricot-site-lb` or `tricot-global-lb` is used
4. Select rule that redirects request to a node that is running in the same site (geographical region, datacenter, whatever) than the node Tricot is running on, based on the metadata key `site` in Consul's node information, if such information is available, except if `tricot-global-lb` is used
5. Round-robin selection of backend nodes

## Knowing what Tricot is doing

Logs are the privileged place to get information about what Tricot is doing. You can set the log level by changing the `RUST_LOG` environment variable. The following log levels are avaialble:

- `RUST_LOG=tricot=warn`: Tricot will only show warning and errors, it should be quite quiet
- `RUST_LOG=tricot=info`: Tricot will dump the proxy configuration (the set of rules it uses to match backends) each time it changes, and will show a line for each incoming request (but it won't indicate which backend the request was routed to). You will also get information about certificate obtention and renewal using Let's Encrypt.
- `RUST_LOG=tricot=debug`: Tricot will show for each request the backend to which it is routed. It will also show all of its interactions with Consul
- `RUST_LOG=tricot=trace`: Tricot will show details such as the request's headers for all request at all stages of proxying.
